<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>


<footer class="fullwidth" role="contentinfo">
	<div class="container">
		<div class="flex-cont">
			<div class="colf footer-logo">
				<a id="logo" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<img class="logo" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" />
				</a>
				<p><small>&copy;Capital One Venture Group Ltd. <br> All Rights Reserved</small></p>
			</div>
			<div class="colf">
				<?php wp_nav_menu( array( 'theme_location' => 'max_mega_menu_1' ) ); ?>
				<p>Contact Information: Suite 3204 - 610 Granville Street, Vancouver B.C. V6C-3T3 <br>
				T: <a href="tel:+16043437740">604.343.7740</a>  |  <a href="mailto:info@cap1vg.com">info@cap1vg.com</a> <br>
				<small>Website by: Vision Advertising &amp; Design</small></p>
			</div>
		</div>
	</div> 
</footer>

<?php wp_footer(); ?>

</body>
</html>